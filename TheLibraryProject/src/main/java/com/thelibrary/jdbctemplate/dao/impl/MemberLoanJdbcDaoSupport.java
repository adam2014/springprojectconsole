package com.thelibrary.jdbctemplate.dao.impl;

import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hsqldb.types.Types;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import com.thelibrary.domain.MemberLoan;
import com.thelibrary.exceptions.LibraryException;
import com.thelibrary.interfaces.impl.LoanImpl;
import com.thelibrary.jdbctemplate.dao.MemberLoanDAO;

/**
 * @author adam abdulrehman R00118124
 *  
 */

@Repository
@Transactional(readOnly = true)
public class MemberLoanJdbcDaoSupport extends JdbcDaoSupport implements
		MemberLoanDAO {

	@Autowired
	private TransactionTemplate transactionTemplate;

	private static final Logger logger = Logger
			.getLogger(MemberLoanJdbcDaoSupport.class);

	@Autowired
	public void setTxTemplate(TransactionTemplate txTemplate) {
		this.transactionTemplate = txTemplate;
	}

	@Autowired
	public MemberLoanJdbcDaoSupport(DataSource dataSource) {
		setDataSource(dataSource);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, timeout = 1000)
	public void createMemberLoan(Integer memberId, LoanImpl loan) {

		Integer bookId = loan.getId();
		Date loanDate = loan.getLoanDate();
		Date returnDate = loan.getReturnDate();
		double fine = loan.getFine();

		String SQL = "INSERT INTO member_loans_Book (Member_id, Book_id, loan_date, return_date, fine) values (?, ?, ?, ?, ?)";

		Object[] params = new Object[] { memberId, bookId, loanDate,
				returnDate, fine };
		PreparedStatementCreatorFactory psc = new PreparedStatementCreatorFactory(
				SQL);
		psc.addParameter(new SqlParameter("memberId", Types.INTEGER));
		psc.addParameter(new SqlParameter("bookId", Types.INTEGER));
		psc.addParameter(new SqlParameter("loanDate", Types.DATE));
		psc.addParameter(new SqlParameter("returnDate", Types.DATE));
		psc.addParameter(new SqlParameter("fine", Types.DOUBLE));

		try {
			this.getJdbcTemplate().update(
					psc.newPreparedStatementCreator(params));
		} catch (Exception e) {
			logger.debug(e.getStackTrace());
			throw new LibraryException();
		}

		logger.debug("createMemberLoan(): generated id is: ");

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, timeout = 1000)
	public int deleteMemberLoanWithMember(Integer memberId) {
		String SQL = "DELETE FROM Member_loans_Book WHERE Member_id = ?";
		return getJdbcTemplate().update(SQL, new Object[] { memberId });
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, timeout = 1000)
	public int deleteMemberLoanWithBook(Integer bookId) {
		String SQL = "DELETE FROM Member_loans_Book WHERE Book_id = ?";
		return getJdbcTemplate().update(SQL, new Object[] { bookId });
	}

	public List<MemberLoan> listMemberLoansWithMember(Integer memberId) {
		List<MemberLoan> memberLoans;
		
		String SQL = "SELECT * FROM member_loans_book "
				+ " JOIN member ON member_loans_book.member_id=member.id "
				+ " JOIN book ON member_loans_book.book_id=book.id "
				+ " WHERE member_loans_book.member_id = ?";
		
		memberLoans = getJdbcTemplate().query(SQL, new Object[] { memberId },
				new MemberLoanMapper());

		return memberLoans;
	}

	public List<MemberLoan> listMemberLoansWithBook(Integer bookId) {

		List<MemberLoan> memberLoans;
		
		String SQL = "SELECT * FROM member_loans_book "
				+ " JOIN member ON member_loans_book.member_id=member.id "
				+ " JOIN book ON member_loans_book.book_id=book.id "
				+ " WHERE member_loans_book.book_id = ?";
		
		memberLoans = getJdbcTemplate().query(SQL, new Object[] { bookId },
				new MemberLoanMapper());

		return memberLoans;

	}

	public List<MemberLoan> listMemberLoans() {
		String SQL = "SELECT * FROM Member_loans_Book"
					+ " JOIN member ON member_loans_book.member_id=member.id "
					+ " JOIN book ON member_loans_book.book_id=book.id ";
		
		List<MemberLoan> memberLoans = getJdbcTemplate().query(SQL,
				new MemberLoanMapper());
		
		return memberLoans;
	}

	public int countRows() {
		String SQL = "SELECT count(*) FROM Member_loans_Book";
		
		int rows = getJdbcTemplate().queryForObject(SQL, Integer.class);
		
		return rows;
	}

}
