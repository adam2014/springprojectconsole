package com.thelibrary.jdbctemplate.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.thelibrary.domain.Book;

/**
 * @author adam abdulrehman R00118124
 *  
 */
public class BookMapper implements RowMapper<Book> {

	public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
		Book book = new Book();

		book.setId(rs.getInt("id"));
		book.setTitle(rs.getString("title"));
		book.setAuthor(rs.getString("author"));
	    book.setIsbn(rs.getString("isbn"));
	    
	    // rs.getDate Retrieves the value of the publication_date column
	    // row of this ResultSet object as a java.sql.Date object in the 
	    // Java programming language. So, conversion is necessary here
	    java.util.Date date = new java.util.Date(rs.getDate("publication_date").getTime());
	    
	    book.setPublicationDate(date);
	    book.setPublisher(rs.getString("publisher"));
	    book.setAvailable(rs.getBoolean("available"));
		return book;
	}

}
