package com.thelibrary.jdbctemplate.dao;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.stereotype.Service;
import com.thelibrary.domain.MemberLoan;
import com.thelibrary.interfaces.impl.LoanImpl;

/**
 * @author adam abdulrehman R00118124
 *  
 */
@Service
public interface MemberLoanDAO {
	public void setDataSource(DataSource ds);

	// This is the method to be used to create a record in the Member_loans_Book table.
	// Returns auto-generated keys
	public void createMemberLoan(Integer memberId, LoanImpl loan);

	// This is the method to be used to delete all records in the Member_loans_Book table
	// given a member_id. Returns the number of rows affected
	public int deleteMemberLoanWithMember(Integer memberId);
	
	// This is the method to be used to delete all records in the Member_loans_Book table
	// given a book_id. Returns the number of rows affected
	public int deleteMemberLoanWithBook(Integer bookId);

	// This is the method to be used to list down all the records from the
	// Member_loans_Book table for the loans of the member with the given id.
	public List<MemberLoan> listMemberLoansWithMember(Integer memberId);
	
	// This is the method to be used to list down all the records from the
	// Member_loans_Book table for the loans of the member with the given id.
	public List<MemberLoan> listMemberLoansWithBook(Integer bookId);

	// This is the method to be used to list down all the records from the
	// Member_loans_Book table 
	public List<MemberLoan> listMemberLoans();
	
	// This is the method to be used to count the number of records from
	// the Member_loans_Book table
	public int countRows();
}
