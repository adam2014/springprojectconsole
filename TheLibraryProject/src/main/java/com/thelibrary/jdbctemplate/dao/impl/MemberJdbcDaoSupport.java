package com.thelibrary.jdbctemplate.dao.impl;

import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hsqldb.types.Types;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import com.thelibrary.domain.Member;
import com.thelibrary.exceptions.LibraryException;
import com.thelibrary.jdbctemplate.dao.MemberDAO;

/**
 * @author adam abdulrehman R00118124
 *  
 */

@Repository
@Transactional(readOnly=true)
public class MemberJdbcDaoSupport extends JdbcDaoSupport implements MemberDAO {

	@Autowired
	private TransactionTemplate transactionTemplate;
	
	private static final Logger logger = Logger.getLogger(MemberJdbcDaoSupport.class);
	
	@Autowired
	public void setTxTemplate(TransactionTemplate txTemplate) {
		this.transactionTemplate = txTemplate;
	}
	
	@Autowired
	public MemberJdbcDaoSupport(DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, timeout = 1000)
	public KeyHolder createMember(String name, String address1, String address2,
			String town, String contactNumber, Integer bookAllowance,
			double balance, boolean active) {
		String SQL = "INSERT INTO member (name, address1, address2, town, contact_number, book_allowance, balance, active) values (?, ?, ?, ?, ?, ?, ?, ?)";

		Object[] params = new Object[] { name, address1, address2, town,
				contactNumber, bookAllowance, balance, active };
		PreparedStatementCreatorFactory psc = new PreparedStatementCreatorFactory(
				SQL);
		psc.addParameter(new SqlParameter("name", Types.VARCHAR));
		psc.addParameter(new SqlParameter("address1", Types.VARCHAR));
		psc.addParameter(new SqlParameter("address2", Types.VARCHAR));
		psc.addParameter(new SqlParameter("town", Types.VARCHAR));
		psc.addParameter(new SqlParameter("contactNumber", Types.VARCHAR));
		psc.addParameter(new SqlParameter("bookAllowance", Types.INTEGER));
		psc.addParameter(new SqlParameter("balance", Types.DOUBLE));
		psc.addParameter(new SqlParameter("active", Types.BIT));

		KeyHolder holder = new GeneratedKeyHolder();
		
		try {
			this.getJdbcTemplate().update(psc.newPreparedStatementCreator(params),
					holder);
		} catch (Exception e) {
			logger.debug(e.getStackTrace());
			
			// provide rollback by throwing an exception
			// this is strictly not required as we already have an exception
			// thrown but as in the class example and as a requirement for
			// the project it is added here. The scope of this assignment
			// really does not necessitate this but here it is.
			// Furthermore, if this was not in a code block on its own
			// i.e within the try catch there would be unreachable code,
			// which, for the purposes of testing is required to return
			// the generated keys
			throw new LibraryException();
		}
	
		logger.debug("createMember(): generated id is: " + holder.getKey().intValue());
		
		return holder;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, timeout = 1000)
	public int deleteMember(Integer memberId) {
		String SQL = "DELETE FROM member WHERE id = ?";
		int rowsAffected;
		
		try {
			rowsAffected = getJdbcTemplate().update(SQL, new Object[] { memberId });
			return rowsAffected;
		} catch (Exception e) {
			logger.debug(e.getStackTrace());
			throw new LibraryException();
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, timeout = 1000)
	public void updateMemberAddress(Integer memberId, String address1,
			String address2) {
		String SQL = "UPDATE member set address1 = ?, address2=? where id = ?";
		
		try {
			getJdbcTemplate().update(SQL, new Object[] { address1, address2, memberId });
		} catch (Exception e) {
			logger.debug(e.getStackTrace());
			throw new LibraryException();
		}
		
		return;

	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, timeout = 1000)
	public int deleteMember(String name) {
		String SQL = "DELETE FROM member WHERE name = ?";
		int rowsAffected;
		
		try {
			rowsAffected = getJdbcTemplate().update(SQL, new Object[] { name });
			return rowsAffected;
		} catch (Exception e) {
			logger.debug(e.getStackTrace());
			throw new LibraryException();
		}
	}

	public List<Member> listMembers() {
		String SQL = "SELECT * FROM member";
		List<Member> members = getJdbcTemplate().query(SQL, new MemberMapper());
		return members;
	}

	// returning a list to make sure that there might be other records present
	// previously loaned books existing. So this method gives the list of those 
	// who've borrowed the book with id given 
	public List<Member> listMember(Integer bookId) {
		List<Member> members;
		String SQL = "SELECT * from member"
				+ " JOIN member_loans_book on member_loans_book.member_id=member.id "
				+ " AND member_loans_book.book_id= ?";
		
		members = getJdbcTemplate().query(SQL, new Object[] { bookId }, new MemberMapper());

		return members;
	}

	public List<Member> listMembers(String bookTitle) {
		List<Member> members;
		
		String SQL = "SELECT * from member"
				+ " JOIN member_loans_book ON member_loans_book.member_id=member.id "
				+ " JOIN book ON member_loans_book.book_id=book.id "
				+ " WHERE book.title= ?";
		members = getJdbcTemplate().query(SQL, new Object[] { bookTitle }, new MemberMapper());

		return members;
	}

	// List Members who have 1, 2 and 3 books on loan.
	public List<Member> listMembers(Integer numberOfBooks) {
		List<Member> members;
	
		String SQL = "SELECT * FROM member"
					+ " JOIN member_loans_book ON member_loans_book.member_id = member.id"
					+ " GROUP BY member_loans_book.member_id"
					+ " HAVING COUNT( member_loans_book.book_id ) BETWEEN 1 AND ?" 
					+ " ORDER BY member_loans_book.member_id";
		
		members = getJdbcTemplate().query(SQL, new Object[] {numberOfBooks}, new MemberMapper());
		
		return members; 
	}

	public int countRows() {
		String SQL = "SELECT count(*) FROM member";
		int rows = getJdbcTemplate().queryForObject(SQL, Integer.class);
		
		logger.debug("Number of rows returned from count is: " + rows);
		
		return rows;
	}

	public Member getMember(int memberId) {
		Member member;
		String SQL = "SELECT * from member WHERE id= ?";
		
		member = getJdbcTemplate().queryForObject(SQL, new Object[] { memberId }, new MemberMapper());
		
		return member;
	}
}
