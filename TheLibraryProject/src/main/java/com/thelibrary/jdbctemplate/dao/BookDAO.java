package com.thelibrary.jdbctemplate.dao;

import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import com.thelibrary.domain.Book;

/**
 * @author adam abdulrehman R00118124
 *  
 */

@Service
public interface BookDAO {

	public void setDataSource(DataSource ds);

	// This is the method to be used to create a record in the Book table.
	public KeyHolder createBook(String title, String author, String publisher,
			Date publicationDate, String isbn, boolean available);

	// This is the method to be used to delete a record in the Book table
	// given an id returning number of rows affected
	public int deleteBook(Integer bookId);

	// This is the method to be used to delete a record in the Book table
	// given a title returning number of rows affected
	public int deleteBook(String title);

	// This is the method to be used to list down all the records from the
	// Book table.
	public List<Book> listBooks();

	// This is the method to be used to list down the records from the
	// Book table which are on loan to the given member id
	public List<Book> listBooks(Integer memberId);

	// This is the method to be used to list down all the records from the
	// Book table which are on loan
	public List<Book> listBooksOnLoan();

	// This is the method to be used to count the number of records from
	// the book table
	public int countRows();
	
	// -----------------Additional methods-----------------
	
	// This is the method to be used to get a record from the Book table
	// given an id returning the found book
	public Book getBook(Integer bookId);
}
