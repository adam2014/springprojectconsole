package com.thelibrary.jdbctemplate.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.thelibrary.domain.Member;

/**
 * @author adam abdulrehman R00118124
 *  
 */

public class MemberMapper implements RowMapper<Member> {

	public Member mapRow(ResultSet rs, int rowNum) throws SQLException {
		Member member = new Member();

		member.setId(rs.getInt("id"));
		member.setName(rs.getString("name"));
		member.setAddress1(rs.getString("address1"));
		member.setAddress2(rs.getString("address2"));
		member.setTown(rs.getString("town"));
		member.setContactNumber(rs.getString("contact_number"));
		member.setBookAllowance(rs.getInt("book_allowance"));
		member.setBalance(rs.getDouble("balance"));
		member.setActive(rs.getBoolean("active"));

		return member;
	}

}
