package com.thelibrary.jdbctemplate.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import com.thelibrary.domain.Book;
import com.thelibrary.domain.Member;
import com.thelibrary.domain.MemberLoan;
import com.thelibrary.interfaces.impl.LoanImpl;

/**
 * @author adam abdulrehman R00118124
 *  
 */
public class MemberLoanMapper implements RowMapper<MemberLoan> {

	public MemberLoan mapRow(ResultSet rs, int rowNum) throws SQLException {

		Member member = new Member();

		member.setId(rs.getInt("member.id"));
		member.setName(rs.getString("member.name"));
		member.setAddress1(rs.getString("member.address1"));
		member.setAddress2(rs.getString("member.address2"));
		member.setTown(rs.getString("member.town"));
		member.setContactNumber(rs.getString("member.contact_number"));
		member.setBookAllowance(rs.getInt("member.book_allowance"));
		member.setBalance(rs.getDouble("member.balance"));
		member.setActive(rs.getBoolean("member.active"));
		
		Book book = new Book();

		book.setId(rs.getInt("book.id"));
		book.setTitle(rs.getString("book.title"));
		book.setAuthor(rs.getString("book.author"));
	    book.setIsbn(rs.getString("book.isbn"));

	    java.util.Date date = new java.util.Date(rs.getDate("book.publication_date").getTime());
	    
	    book.setPublicationDate(date);
	    book.setPublisher(rs.getString("book.publisher"));
	    book.setAvailable(rs.getBoolean("book.available"));

	    Date loanDate = new java.util.Date(rs.getDate("member_loans_book.loan_date").getTime());
	    
	    // may return a null value so we have to deal with it
	    // this database field does allow for null
	    Date returnDate = null;
	    if (rs.getInt("member_loans_book.return_date") != 0) {
	    	returnDate = new java.util.Date(rs.getDate("member_loans_book.return_date").getTime()); 
		}
	    
	    
	    double fine = rs.getDouble("member_loans_book.fine");
	    LoanImpl loan = new LoanImpl(book, loanDate, returnDate, fine);
	    
		MemberLoan memberLoan = new MemberLoan(member, loan);
		
		return memberLoan;
	}

}
