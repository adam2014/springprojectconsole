package com.thelibrary.jdbctemplate.dao.impl;

import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.hsqldb.types.Types;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import com.thelibrary.domain.Book;
import com.thelibrary.exceptions.LibraryException;
import com.thelibrary.jdbctemplate.dao.BookDAO;

/**
 * @author adam abdulrehman R00118124
 *  
 */

@Repository
@Transactional(readOnly=true)
public class BookJdbcDaoSupport extends JdbcDaoSupport implements BookDAO {

	@Autowired
	private TransactionTemplate transactionTemplate;
	
	private static final Logger logger = Logger.getLogger(BookJdbcDaoSupport.class);
	
	@Autowired
	public void setTxTemplate(TransactionTemplate txTemplate) {
		this.transactionTemplate = txTemplate;
	}
	
	@Autowired
	public BookJdbcDaoSupport(DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
	public KeyHolder createBook(String title, String author, String publisher,
			Date publicationDate, String isbn, boolean available) {
		String SQL = "INSERT INTO book (title, author, publisher, publication_date, isbn, available) values (?, ?, ?, ?, ?, ?)";
		
		Object[] params = new Object[] { title, author, publisher, publicationDate, isbn, available };
		PreparedStatementCreatorFactory psc = new PreparedStatementCreatorFactory(
				SQL);
		psc.addParameter(new SqlParameter("title", Types.VARCHAR));
		psc.addParameter(new SqlParameter("author", Types.VARCHAR));
		psc.addParameter(new SqlParameter("publisher", Types.VARCHAR));
		psc.addParameter(new SqlParameter("publicationDate", Types.DATE));
		psc.addParameter(new SqlParameter("isbn", Types.VARCHAR));
		psc.addParameter(new SqlParameter("available", Types.BIT));

		KeyHolder holder = new GeneratedKeyHolder();
		
		try {
			this.getJdbcTemplate().update(psc.newPreparedStatementCreator(params),
					holder);
		} catch (Exception e) {
			logger.error(e.getMessage());
			// provide rollback by throwing an exception
			// this is strictly not required as we already have an exception
			// thrown but as in the class example and as a requirement for
			// the project it is added here. The scope of this assignment
			// really does not necessitate this but here it is.
			// Furthermore, if this was not in a code block on its own
			// i.e within the try catch there would be unreachable code,
			// which, for the purposes of testing is required to return
			// the generated keys
			throw new LibraryException();
		}
	
		logger.debug("createBook: generated id is: " + holder.getKey().intValue());
		return holder;	
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, timeout = 1000)
	public int deleteBook(Integer bookId) {
		String SQL = "DELETE FROM book WHERE id = ?";
		int rowsAffected;
		
		try {
			rowsAffected = getJdbcTemplate().update(SQL, new Object[] { bookId });
		} catch (Exception e) {
			logger.debug(e.getStackTrace());
			
			// provide rollback by throwing an exception.
			// it feels wrong because an exception's already thrown
			// abit overkill but it atleast demonstrates my understanding
			throw new LibraryException();
		}
		logger.debug("deleteBook ("+ bookId + "): affected rows: " + rowsAffected);
		
		return rowsAffected;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW, isolation = Isolation.DEFAULT, timeout = 1000)
	public int deleteBook(String title) {
		String SQL = "DELETE FROM book WHERE title = ?";
		
		int rowsAffected;
		
		try {
			rowsAffected = getJdbcTemplate().update(SQL, new Object[] { title });
		} catch (Exception e) {
			logger.debug(e.getStackTrace());
			
			// provide rollback by throwing an exception.
			// it feels wrong because an exception's already thrown
			// abit overkill but it atleast demonstrates my understanding
			throw new LibraryException();
		}
		
		logger.debug("deleteBook ("+ title + "): affected rows: " + rowsAffected);
		
		return rowsAffected;
	}

	public List<Book> listBooks() {
		String SQL = "SELECT * FROM book";
		
		List<Book> books = getJdbcTemplate().query(SQL, new BookMapper());
		
		logger.debug("listBooks(): returned: " + books.size() + " books");
		
		return books;
	}

	public List<Book> listBooks(Integer memberId) {
		List<Book> books;
		String SQL = "SELECT * from book"
				+ " JOIN member_loans_book on member_loans_book.book_id=book.id "
				+ " AND member_loans_book.member_id= ? GROUP BY book.id";
		books = getJdbcTemplate().query(SQL, new Object[] { memberId }, new BookMapper());

		logger.debug("listBooks(" + memberId + "): returned: " + books.size() + " books");
		
		return books;
	}

	public List<Book> listBooksOnLoan() {
		List<Book> books;
		String SQL = "SELECT * from book"
				+ " JOIN member_loans_book on member_loans_book.book_id=book.id  GROUP BY book.id";
		
		books = getJdbcTemplate().query(SQL, new BookMapper());
		
		logger.debug("listBooksOnLoan(): returned: " + books.size() + " books");
		
		return books;
	}

	public int countRows() {
		String SQL = "SELECT count(*) FROM book";
		int rows = getJdbcTemplate().queryForObject(SQL, Integer.class);
		logger.debug("countRows(): number of rows: " + rows);
		
		return rows;
	}

	public Book getBook(Integer bookId) {
		Book book;
		String SQL = "SELECT * from book WHERE id= ?";
		book = getJdbcTemplate().queryForObject(SQL, new Object[] { bookId }, new BookMapper());
		return book;
	}
}
