package com.thelibrary.jdbctemplate.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import com.thelibrary.domain.Member;

/**
 * @author adam abdulrehman R00118124
 *  
 */

@Service
public interface MemberDAO {

	public void setDataSource(DataSource ds);

	// This is the method to be used to create a record in the Member table.
	// Returns auto-generated keys
	public KeyHolder createMember(String name, String address1,
			String address2, String town, String contactNumber,
			Integer bookAllowance, double balance, boolean active);

	// This is the method to be used to delete a record in the Member table
	// given an id. Returns the number of rows affected
	public int deleteMember(Integer memberId);

	// This is the method to be used to update a record in the Member table
	// given an id. The column to be updated is the address1 and address2
	// columns
	public void updateMemberAddress(Integer memberId, String address1,
			String address2);

	// This is the method to be used to delete a record in the Member table
	// given a name. Returns the number of rows affected
	public int deleteMember(String name);

	// This is the method to be used to list down all the records from the
	// Member table.
	public List<Member> listMembers();

	// This is the method to be used to list down the record from the
	// Member table which has the book on loan with given book id
	// returning a list to make sure that there might be other records present
	// previously loaned books existing. So this method gives the list of those 
	// who've borrowed the book with id given 
	public List<Member> listMember(Integer bookId);

	// This is the method to be used to list down all the records from the
	// Member table which have the book on loan with given title
	public List<Member> listMembers(String bookTitle);

	// This is the method to be used to list down all the records from the
	// Member table which have less than number passed of books or less
	public List<Member> listMembers(Integer numberOfBooks);
	
	// This is the method to be used to count the number of records from
	// the member table
	public int countRows();
	
	// -----------------Additional methods-----------------
	
	// This is the method to be used to get a record in the Member table
	// given an id returning the found member with that id
	public Member getMember(int memberId);

}
