package com.thelibrary.interfaces;

import java.util.Date;

import org.springframework.stereotype.Service;

/**
 * @author adam abdulrehman R00118124
 *  
 */

@Service
public interface Loan {

	// allows for the return of a borrowed book by setting the return date and
	// available field of the book to true
	public void returnBook(Date returnDate);

	// allows for the application of a fine
	public void applyFine(double fine);

}
