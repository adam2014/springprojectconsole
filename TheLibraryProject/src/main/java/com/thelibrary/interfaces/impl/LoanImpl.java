package com.thelibrary.interfaces.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.thelibrary.domain.Book;
import com.thelibrary.interfaces.Loan;

/*
 *  This class is provided for demonstration purposes
 *  I don't think it was required, nevertheless, here it is!
 *  
 * @Author Adan Abdulrehman
 */

@Component
public class LoanImpl extends Book implements Loan{

	Date loanDate;

	Date returnDate;

	@Value("0.00")
	double fine;

	public LoanImpl() {

	}

	public LoanImpl(Book book, Date loanDate, Date returnDate, double fine) {

		super(book.getId(), book.getTitle(), book.getAuthor(), 
			  book.getPublisher(), book.getPublicationDate(), book.getIsbn(),
			  book.isAvailable());
		
		this.loanDate = loanDate;
		this.returnDate = returnDate;
		this.fine = fine;
	}

	public LoanImpl(Book book, Date loanDate) {

		super(book.getId(), book.getTitle(), book.getAuthor(), 
			  book.getPublisher(), book.getPublicationDate(), book.getIsbn(),
			  book.isAvailable());
		
		this.loanDate = loanDate;
	}
	
	public Date getLoanDate() {
		return loanDate;
	}

	@Required
	public void setLoanDate(Date loanDate) {
		this.loanDate = loanDate;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public double getFine() {
		return fine;
	}

	@Required
	public void setFine(double fine) {
		this.fine = fine;
	}
	
	// toString method here not tidy but it'll do for testing purposes
	@Override
	public String toString() {
		return "Loan [Date loaned=" + loanDate + ", Return Date=" + returnDate
				+ ", Fine=" + fine + ", Book Id=" + getId() + ", Book Title="
				+ getTitle() + ", Author=" + getAuthor() + ", Isbn="
				+ getIsbn() + "]";
	}

	// implemented method
	public void returnBook(Date returnDate) {
		this.returnDate = returnDate;
		setAvailable(true); 
	}

	// implemented method
	public void applyFine(double fine) {
		this.fine = fine;	
	}

}
