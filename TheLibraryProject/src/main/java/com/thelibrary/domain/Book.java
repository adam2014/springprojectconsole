package com.thelibrary.domain;

import java.util.Date;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/*
 *  This is the POJO that represents a book entity.
 *  Note: Setters that have the @Required annotation
 *        are ones with a database field with NOT NULL.
 *        Isbn is not set as @Required because its 
 *        equivalent in the database is allowed to be NULL 
 *       
 * @author adam abdulrehman R00118124
 * 
 */
@Component("book")
public class Book {
	private int id;
	private String title;
	private String author;
	private String publisher;
	private Date publicationDate;
	private String isbn;
	
	// Demonstrating you can also apply default values to fields
	@Value("true") 
	private boolean available;

	public Book() {

	}

	// Demonstrating @value for default values from within a constructor argument
	public Book(int id, String title, String author, String publisher,
			Date publicationDate, String isbn, @Value("true") boolean available) {
		this(title, author, publisher, publicationDate, isbn, available);
		this.id = id;
	}

	public Book(String title, String author, String publisher,
			Date publicationDate, String isbn, @Value("true") boolean available) {
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.publicationDate = publicationDate;
		this.isbn = isbn;
		this.available = available;
	}
	
	// This is added so as to show in usage of the default value for available field
	public Book(String title, String author, String publisher,
			Date publicationDate, String isbn) {
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.publicationDate = publicationDate;
		this.isbn = isbn;
	}

	public int getId() {
		return id;
	}

	@Required
	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	@Required
	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	@Required
	public void setAuthor(String author) {
		this.author = author;
	}

	public String getPublisher() {
		return publisher;
	}

	@Required
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

	public Date getPublicationDate() {
		return publicationDate;
	}

	public void setPublicationDate(Date publicationDate) {
		this.publicationDate = publicationDate;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public boolean isAvailable() {
		return available;
	}

	@Required
	public void setAvailable(boolean available) {
		this.available = available;
	}

}
