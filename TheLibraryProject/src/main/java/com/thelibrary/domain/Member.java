package com.thelibrary.domain;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author adam abdulrehman R00118124
 *  
 */

@Component
public class Member {

	private int id;
	private String name;
	private String address1;
	private String address2;
	private String town;
	private String contactNumber;
	
	@Value("3")
	private int bookAllowance;
	
	@Value("0.00")
	private double balance;
	
	@Value("true")
	private boolean active;

	public Member() {
	}
	
	
	public Member(int id, String name, String address1, String address2,
			String town, String contactNumber,@Value("3")int bookAllowance,
			double balance, @Value("true")boolean active) {
		this(name, address1, address2, town,
				contactNumber, bookAllowance, balance, active);
		this.id = id;
	}


	public Member(String name, String address1, String address2, String town,
			String contactNumber, @Value("4")int bookAllowance, @Value("0.0")double balance,
			@Value("true")boolean active) {
		this.name = name;
		this.address1 = address1;
		this.address2 = address2;
		this.town = town;
		this.contactNumber = contactNumber;
		this.bookAllowance = bookAllowance;
		this.balance = balance;
		this.active = active;
	}

	public Member(String name, String address1, String address2, String town,
			String contactNumber, @Value("4")int bookAllowance, @Value("0.0")double balance) {
		this.name = name;
		this.address1 = address1;
		this.address2 = address2;
		this.town = town;
		this.contactNumber = contactNumber;
		this.bookAllowance = bookAllowance;
		this.balance = balance;
	}
	
	public int getId() {
		return id;
	}

	@Required
	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	@Required
	public void setName(String name) {
		this.name = name;
	}

	public String getAddress1() {
		return address1;
	}

	@Required
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getTown() {
		return town;
	}

	@Required
	public void setTown(String town) {
		this.town = town;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	@Required
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public int getBookAllowance() {
		return bookAllowance;
	}

	@Required
	public void setBookAllowance(int bookAllowance) {
		this.bookAllowance = bookAllowance;
	}

	public double getBalance() {
		return balance;
	}

	@Required
	public void setBalance(double balance) {
		this.balance = balance;
	}

	public boolean isActive() {
		return active;
	}

	@Required
	public void setActive(boolean active) {
		this.active = active;
	}
}
