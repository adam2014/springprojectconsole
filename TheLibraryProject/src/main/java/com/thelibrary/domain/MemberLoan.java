package com.thelibrary.domain;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Component;

import com.thelibrary.interfaces.impl.LoanImpl;

/*
 *  This class is provided to demonstrate Spring features
 *  
 * @Author adan abdulrehman
 */

@Component("memberLoan")
public class MemberLoan extends Member {

	private LoanImpl loan;

	public MemberLoan() {
	}

	public MemberLoan(Member member, LoanImpl loan) {

		super(member.getId(), member.getName(), member.getAddress1(), 
				member.getAddress2(), member.getTown(), member.getContactNumber(),
				member.getBookAllowance(), member.getBalance(), 
				member.isActive());
		this.loan = loan;
	}
	
	public MemberLoan(int id, Member member, LoanImpl loan) {

		super(member.getId(), member.getName(), member.getAddress1(), 
				member.getAddress2(), member.getTown(), member.getContactNumber(),
				member.getBookAllowance(), member.getBalance(), 
				member.isActive());
		this.loan = loan;
	}

	public LoanImpl getLoan() {
		return loan;
	}

	@Required
	public void setLoan(LoanImpl loan) {
		this.loan = loan;
	}
}
