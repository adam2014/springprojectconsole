package com.thelibrary.jdbctemplate.dao.impl;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.thelibrary.domain.Book;

/**
 * @author adam abdulrehman R00118124
 * 
 * JUnit Test for BookJdbcDaoSupport 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:configuration.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class BookJdbcDaoSupportTest {

	// since the applicationContext is autowired there is no need for
	// setting up the bean
	@Autowired
	ApplicationContext applicationContext;

	// since the BookJdbcDaoSupport is autowired there is no need for
	// setting up the bean
	@Autowired
	BookJdbcDaoSupport bookJdbcDaoSupport;

	static final int NUMBER_OF_MEMBERS = 7;
	static final int NUMBER_OF_BOOKS = 10;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link com.thelibrary.jdbctemplate.dao.impl.BookJdbcDaoSupport#createBook(java.lang.String, java.lang.String, java.lang.String, java.util.Date, java.lang.String, boolean)}
	 * . expected output is the book id of the newly created book
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testCreateBook() {

		// book values to be used for creation
		final String actualTitle = "Lord Of the ring";
		final String actualAuthor = "JRTolkien";
		final String actualPublisher = "APress";

		Calendar calendar = Calendar.getInstance();
		calendar.set(2012, 12, 12, 0, 0, 0);
		java.util.Date actualPublicationDate = calendar.getTime();

		final String actualIsbn = "112333";
		final boolean actualAvailability = true;

		// get the number of rows in the db
		int rowCountBeforeCreate = bookJdbcDaoSupport.countRows();
			
		// create the book
		KeyHolder ids = null;
		
		try {
			 ids = bookJdbcDaoSupport.createBook(actualTitle,
						actualAuthor, actualPublisher, actualPublicationDate,
						actualIsbn, actualAvailability);
			
		} catch (DataAccessException ex) {
			fail("getting the book threw an exception when it was expected not to");
		}

		// get the number of rows in the db
		int actualRowCount = bookJdbcDaoSupport.countRows();
		assertEquals("Number of rows is not as expected",
				rowCountBeforeCreate + 1, actualRowCount);

		// test that a key holder is not null
		assertNotNull("keys is null", ids);
				
		// test that a generated key is returned
		assertNotNull("returned keys is null", ids.getKey());

		// test that the rows have increased by one
		assertEquals("rows don't much", NUMBER_OF_BOOKS + 1,
				bookJdbcDaoSupport.countRows());

		// test the book inserted
		Book book = bookJdbcDaoSupport.getBook(ids.getKey().intValue());
		assertNotNull("inserted book is null", book);
		assertEquals("inserted book title is not the same as expected",
				actualTitle, book.getTitle());
		assertEquals("inserted book author is not the same as expected",
				actualAuthor, book.getAuthor());
		assertEquals("inserted book publisher is not the same as expected",
				actualPublisher, book.getPublisher());
		assertEquals("inserted publication date is not the same as expected",
				actualPublicationDate.toString(), book.getPublicationDate()
						.toString());
		assertEquals("inserted book isbn is not the same as expected",
				actualIsbn, book.getIsbn());
		assertEquals("inserted book availabilty is not the same as expected",
				actualAvailability, book.isAvailable());
	}

	/**
	 * Test method for
	 * {@link com.thelibrary.jdbctemplate.dao.impl.BookJdbcDaoSupport#deleteBook(java.lang.Integer)}
	 * . expected output is 1 row affected from the deletion
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testDeleteBookById() {
		// book id of book under delete test
		final int bookId = 7;

		// check if the book exists first and get the details of the Book
		Book book = bookJdbcDaoSupport.getBook(bookId);

		// verify that we have the book
		assertNotNull("inserted book is null", book);

		// verify that an id is present and same as what we asked for
		// using assertTrue to demonstrate the assert but could have used
		// asserEquals
		assertTrue("Book id is not as expected", book.getId() == bookId);

		// get the number of rows in the db
		int rowCountBeforeDelete = bookJdbcDaoSupport.countRows();

		// Delete the book passing in the id of the obtained book
		int affectedRows = bookJdbcDaoSupport.deleteBook(book.getId());

		// get the number of rows in the db
		int actualRowCount = bookJdbcDaoSupport.countRows();
		assertEquals("Number of rows is not as expected",
				rowCountBeforeDelete - 1, actualRowCount);

		// Verify that 1 row has been affected
		assertEquals("affected rows is not the same as expected", 1,
				affectedRows);

		// verify that the book does not now exist anymore in the database
		// after deletion by getting it
		Book deletedbook = null;
		try {
			deletedbook = bookJdbcDaoSupport.getBook(book.getId());
			fail("getting the book didn't throw an exception when it was expected to");
		} catch (DataAccessException ex) {
			assertNotNull("Exception should have been caughtbut it didn't", ex);
		}

		assertNull("deleted book still exists and it shouldn't", deletedbook);
	}

	/**
	 * Test method for
	 * {@link com.thelibrary.jdbctemplate.dao.impl.BookJdbcDaoSupport#deleteBook(java.lang.String)}
	 * . expected output is 1 row affected from the deletion
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testDeleteBookByTitle() {
		// book id of book under delete test
		final String expectedBookTitle = "book 8";
		final int expectedBookId = 8;

		// check if the book exists first and get the details of the Book
		Book book = bookJdbcDaoSupport.getBook(expectedBookId);

		// verify that we have a book with id supplied
		assertNotNull("inserted book is null", book);

		// verify that the book title is as expectedBookTitle variable
		assertEquals("obtained book is not same as we expect",
				expectedBookTitle, book.getTitle());

		// verify that the book id is as expectedBookId
		assertEquals("obtained book is not same as we expect", 8, book.getId());

		// get the number of rows in the db
		int rowCountBeforeDelete = bookJdbcDaoSupport.countRows();

		// Delete the book passing in the id of the obtained book
		int affectedRows = bookJdbcDaoSupport.deleteBook(book.getTitle());

		// get the number of rows in the db
		int actualRowCount = bookJdbcDaoSupport.countRows();
		assertEquals("Number of rows is not as expected",
				rowCountBeforeDelete - 1, actualRowCount);

		// Verify that 1 row has been affected
		assertEquals("affected rows is not the same as expected", 1,
				affectedRows);

		// verify that the book does not now exist anymore in the database
		// after deletion by getting it
		Book deletedbook = null;
		try {
			deletedbook = bookJdbcDaoSupport.getBook(8);
			fail("getting the book didn't throw an exception when it was expected to");
		} catch (DataAccessException ex) {
			assertNotNull("Exception should have been caughtbut it didn't", ex);
		}

		assertNull("deleted book still exists and it shouldn't", deletedbook);

	}

	/**
	 * Test method for
	 * {@link com.thelibrary.jdbctemplate.dao.impl.BookJdbcDaoSupport#listBooks()}
	 * Expected output is a list of all books
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testListBooks() {

		// book rows in the database
		int expectedRows = 10;
		
		// get the number of rows in the db
		int actualRows = bookJdbcDaoSupport.countRows();
		List<Book> books = bookJdbcDaoSupport.listBooks();
		
		// Verify that the number of rows his as expected
		assertEquals("actual rows is not the same as expected", actualRows, books.size());
		assertEquals("actual rows is not the same as expected", expectedRows, books.size());
		
		// check that list contains not nulls, and id's not empty
		for (Book book : books) {
			assertNotNull("List shouldn't contain  nulls", book);
			assertTrue("id of object isn't correct", book.getId() > 0 && book.getId() < 11);
		}	
	}

	/**
	 * Test method for
	 * {@link com.thelibrary.jdbctemplate.dao.impl.BookJdbcDaoSupport#listBooks(java.lang.Integer)}
	 * 
	 * The method under test to returns a list of the records from the
	 * Book table which are on loan to the given member id
	 * Expected output is the list of books on loan to the given member id
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testListBooksForMember() {
		
		// member id for the member to obtain list of books on loan
		int memberIdWithBooks = 3;
		// member id 3 has 5 unique books on loan
		int numberOfBooksOnLoan = 5;
		// ids of the books
		int [] expectedIdsArray = {1, 3, 5, 7, 10};
		
		// call the method under test
		List<Book>books = bookJdbcDaoSupport.listBooks(memberIdWithBooks);
		
		// Verify that the number of books is as expected
		assertEquals("actual rows is not the same as expected", numberOfBooksOnLoan, books.size());
				
		// run asserts on the individual book
		for (Book book : books) {
			assertNotNull("List shouldn't contain  nulls", book);
			assertTrue("id of book isn't correct", book.getId() > 0 && book.getId() < 11);
			assertTrue("expected id of book isn't in the list returned", Arrays.binarySearch(expectedIdsArray, book.getId()) >= 0);
		}
		
		// test a member who has no books
		memberIdWithBooks = 5;
		numberOfBooksOnLoan = 0;
		
		// call the method under test
		books = bookJdbcDaoSupport.listBooks(memberIdWithBooks);
				
		// Verify that the number of books is as expected
		assertEquals("actual rows is not the same as expected", numberOfBooksOnLoan, books.size());
	}

	/**
	 * Test method for
	 * {@link com.thelibrary.jdbctemplate.dao.impl.BookJdbcDaoSupport#listBooksOnLoan()}
	 * 
	 * The method under test to returns a list of the records from the
	 * Book table which are on loan
	 * Expected output is the list of books on loan
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testListBooksOnLoan() {

		// member id 3 has 5 unique books on loan
		int numberOfBooksOnLoan = 9;
		// ids of the books
		int [] expectedIdsArray = {1, 2, 3, 5, 6, 7, 8, 9, 10};
		
		// call the method under test
		List<Book>books = bookJdbcDaoSupport.listBooksOnLoan();
		
		// Verify that the number of books is as expected
		assertEquals("actual rows is not the same as expected", numberOfBooksOnLoan, books.size());
				
		// run asserts on the individual book
		for (Book book : books) {
			assertNotNull("List shouldn't contain  nulls", book);
			assertTrue("id of book isn't correct", book.getId() > 0 && book.getId() < 11);
			assertTrue("expected id of book isn't in the list returned", Arrays.binarySearch(expectedIdsArray, book.getId()) >= 0);
		}
	}

	/**
	 * Test method for
	 * {@link com.thelibrary.jdbctemplate.dao.impl.BookJdbcDaoSupport#countRows()}
	 * .
	 * Expected output is that the number of rows will be the same as expected
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testCountRows() {
		
		// book rows in the database
		int expectedRows = 10;
		
		// get the number of rows in the db
		int actualRows = bookJdbcDaoSupport.countRows();
		
		// Verify that the number of rows his as expected
		assertEquals("actual rows is not the same as expected", expectedRows, actualRows);
	}

}
