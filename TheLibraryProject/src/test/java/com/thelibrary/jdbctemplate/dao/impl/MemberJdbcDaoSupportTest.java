/**
 * 
 */
package com.thelibrary.jdbctemplate.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.thelibrary.domain.Member;

/**
 * @author adam abdulrehman R00118124
 * 
 * JUnit Test for MemberJdbcDaoSupport 
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:configuration.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class MemberJdbcDaoSupportTest {

	// since the applicationContext is autowired there is no need for
	// setting up the bean
	@Autowired
	ApplicationContext applicationContext;

	// since the MemberJdbcDaoSupport is autowired there is no need for
	// setting up the bean
	@Autowired
	MemberJdbcDaoSupport memberJdbcDaoSupport;

	static final int NUMBER_OF_MEMBERS = 7;
	static final int NUMBER_OF_BOOKS = 10;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.thelibrary.jdbctemplate.dao.impl.MemberJdbcDaoSupport#createMember(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, double, boolean)}.
	 * This is the test method for createMember which is to be used to create a record in the Member table.
     * Returns auto-generated keys.
     * 
     * The expected output is that the newly created member is inserted into the database.
     * The database rows should increase by one and the details of the new member
     * should exist in the table
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testCreateMember() {
		// member variables to be used for creation
		final String actualName = "Harry";
		final String actualAddress1 = "45";
		final String actualAddress2 = "willowbog farm road";
		final String actualTown = "Athy";
		final String actualContactNumber = "654321";
		final int actualBookAllowance = 6;
		final double actualBalance = 32.89;
		final boolean actualActive = true;

		// get the number of rows in the db
		int rowCountBeforeCreate = memberJdbcDaoSupport.countRows();

		// create the member
		KeyHolder ids = memberJdbcDaoSupport.createMember(actualName, actualAddress1, actualAddress2, actualTown, actualContactNumber, actualBookAllowance, actualBalance, actualActive);

		// get the number of rows in the db
		int actualRowCount = memberJdbcDaoSupport.countRows();
		assertEquals("Number of rows is not as expected",
				rowCountBeforeCreate + 1, actualRowCount);

		// test that a generated key is returned
		assertNotNull("returned keys is null", ids.getKey());

		// test that the rows have increased by one
		assertEquals("rows don't match as expected", NUMBER_OF_MEMBERS + 1,
				memberJdbcDaoSupport.countRows());

		// test the member inserted
		Member member = memberJdbcDaoSupport.getMember(ids.getKey().intValue());
		assertNotNull("inserted member is null", member);
		assertEquals("inserted member name is not the same as expected",
				actualName, member.getName());
		assertEquals("inserted member address1 is not the same as expected",
				actualAddress1, member.getAddress1());
		assertEquals("inserted member address2 is not the same as expected",
				actualAddress2, member.getAddress2());
		assertEquals("inserted member town is not the same as expected",
				actualTown, member.getTown());
		assertEquals("inserted member contact number is not the same as expected",
				actualContactNumber, member.getContactNumber());
		assertEquals("inserted member book allowance is not the same as expected",
				actualBookAllowance, member.getBookAllowance());
		assertTrue("inserted member balance is not the same as expected",
				actualBalance == member.getBalance());
		assertEquals("inserted member active is not the same as expected",
				actualActive, member.isActive());
	}

	/**
	 * Test method for {@link com.thelibrary.jdbctemplate.dao.impl.MemberJdbcDaoSupport#deleteMember(java.lang.Integer)}.
	 * Method under test is the method to be used to delete a record in the Member table
	 * given an id. Returns the number of rows affected.
	 * 
	 * Expected output is that the member with the given id will be deleted. The number of
	 * rowss in the database should decrease by one and the member should not be in the db
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testDeleteMemberById() {
		// member id of member under delete test
		final int memberId = 7;

		// check if the member exists first and get the details of the member
		Member member = memberJdbcDaoSupport.getMember(memberId);

		// verify that we have the member
		assertNotNull("inserted member is null", member);

		// verify that an id is present and same as what we asked for
		// using assertTrue to demonstrate the assert but could have used
		// asserEquals
		assertTrue("member id is not as expected", member.getId() == memberId);

		// get the number of rows in the db
		int rowCountBeforeDelete = memberJdbcDaoSupport.countRows();

		// Delete the member passing in the id of the obtained member
		int affectedRows = memberJdbcDaoSupport.deleteMember(member.getId());

		// get the number of rows in the db
		int actualRowCount = memberJdbcDaoSupport.countRows();
		assertEquals("Number of rows is not as expected",
				rowCountBeforeDelete - 1, actualRowCount);

		// Verify that 1 row has been affected
		assertEquals("affected rows is not the same as expected", 1,
				affectedRows);

		// verify that the member does not now exist anymore in the database
		// after deletion by getting the member
		Member deletedMember = null;
		try {
			deletedMember = memberJdbcDaoSupport.getMember(member.getId());
			fail("getting the member didn't throw an exception when it was expected to");
		} catch (DataAccessException ex) {
			assertNotNull("Exception should have been caught but it didn't", ex);
		}

		assertNull("deleted member still exists and they shouldn't", deletedMember);
	}

	/**
	 * Test method for {@link com.thelibrary.jdbctemplate.dao.impl.MemberJdbcDaoSupport#updateMemberAddress(java.lang.Integer, java.lang.String, java.lang.String)}.
	 * method under test is the method to be used to update a record in the Member table
	 * given an id. The column to be updated is the address1 and address2
	 * columns.
	 * 
	 * Expected output is that for the given member id the record will be updated with the new
	 * address1 and address2
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testUpdateMemberAddress() { 
		
		String address1Change = "changed address1";
		String address2Change = "changed address2";
		
		// member to be used for testing the address update
		Member member = new Member(3, "Mark Little", "Phibsborough", "Dublin 3", "Dublin", "087924273", 4, 22, true);
		
		// check that the member exists and that values are as we have in member local
		Member dbMember = memberJdbcDaoSupport.getMember(member.getId());
		assertNotNull("database member is null", dbMember);
		assertEquals("database member id is not the same as expected",
				member.getId(), dbMember.getId());
		assertEquals("database member name is not the same as expected",
				member.getName(), dbMember.getName());
		assertEquals("database member address1 is not the same as expected",
				member.getAddress1(), dbMember.getAddress1());
		assertEquals("database member address2 is not the same as expected",
				member.getAddress2(), dbMember.getAddress2());
		assertEquals("database member town is not the same as expected",
				member.getTown(), dbMember.getTown());
		assertEquals("database member contact number is not the same as expected",
				member.getContactNumber(), dbMember.getContactNumber());
		assertEquals("database member book allowance is not the same as expected",
				member.getBookAllowance(), dbMember.getBookAllowance());
		assertTrue("database member balance is not the same as expected",
				member.getBalance() == dbMember.getBalance());
		assertEquals("database member active is not the same as expected",
				member.isActive(), dbMember.isActive());
		
		// change the local member address1 and address2 fields
		member.setAddress1(address1Change);
		member.setAddress2(address2Change);
		
		// test that local member address1 and address2 are as we expected
		assertEquals("local member address1 is not the same as expected",
				address1Change, member.getAddress1());
		assertEquals("local member address2 is not the same as expected",
				address2Change, member.getAddress2());
		
		try {
			
			// update the database record with the new changes to adress1 and address2
			memberJdbcDaoSupport.updateMemberAddress(member.getId(), member.getAddress1(), member.getAddress2());
			
			// test the member updated that the update occured
			Member updatedMember = memberJdbcDaoSupport.getMember(member.getId());
			assertNotNull("database member is null", updatedMember);
			assertEquals("database member id is not the same as expected",
					member.getId(), updatedMember.getId());
			assertEquals("database member name is not the same as expected",
					member.getName(), updatedMember.getName());
			assertEquals("database member address1 is not the same as expected",
					member.getAddress1(), updatedMember.getAddress1());
			assertEquals("database member address2 is not the same as expected",
					member.getAddress2(), updatedMember.getAddress2());
			assertEquals("database member town is not the same as expected",
					member.getTown(), updatedMember.getTown());
			assertEquals("database member contact number is not the same as expected",
					member.getContactNumber(), updatedMember.getContactNumber());
			assertEquals("database member book allowance is not the same as expected",
					member.getBookAllowance(), updatedMember.getBookAllowance());
			assertTrue("database member balance is not the same as expected",
					member.getBalance() == updatedMember.getBalance());
			assertEquals("database member active is not the same as expected",
					member.isActive(), updatedMember.isActive());
		} catch (DataAccessException ex) {
			fail("updating the member threw an exception when it was expected not to");
		}
	}

	/**
	 * Test method for {@link com.thelibrary.jdbctemplate.dao.impl.MemberJdbcDaoSupport#deleteMember(java.lang.String)}.
	 * The method under test is the method to be used to delete a record in the Member table
	 * given a name. Returns the number of rows affected
	 * 
	 * Expected output is that a record in the Member table with a member of given name
	 * will be deleted. 
	 * The returned number of rows should be one less than the original number of rows
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testDeleteMemberByName() {
		// member id of member under delete test
		final int memberId = 7;
		final String memberName = "Tina Malone";

		// check if the member exists first and get the details of the member
		Member member = memberJdbcDaoSupport.getMember(memberId);

		// verify that we have the member
		assertNotNull("inserted member is null", member);

		// verify that the member name expected is present and same as local name
		assertTrue("member id is not as expected", member.getName().compareTo(memberName) == 0);

		// get the number of rows in the db
		int rowCountBeforeDelete = memberJdbcDaoSupport.countRows();

		// Delete the member passing in the name of the obtained member
		int affectedRows = memberJdbcDaoSupport.deleteMember(member.getName());

		// get the number of rows in the db
		int actualRowCount = memberJdbcDaoSupport.countRows();
		assertEquals("Number of rows is not as expected",
				rowCountBeforeDelete - 1, actualRowCount);

		// Verify that 1 row has been affected
		assertEquals("affected rows is not the same as expected", 1,
				affectedRows);

		// verify that the member does not now exist anymore in the database
		// after deletion by getting the member
		Member deletedMember = null;
		try {
			deletedMember = memberJdbcDaoSupport.getMember(member.getId());
			fail("getting the member didn't throw an exception when it was expected to");
		} catch (DataAccessException ex) {
			assertNotNull("Exception should have been caught but it didn't", ex);
		}

		assertNull("deleted member still exists and they shouldn't", deletedMember);
	}

	/**
	 * Test method for {@link com.thelibrary.jdbctemplate.dao.impl.MemberJdbcDaoSupport#listMembers()}.
	 * Method under test is the method to be used to list down all the records from the
	 * Member table.
	 * 
	 * Expected output is the list of members which should have the same size as expected
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testListMembers() {
		// member rows in the database
		int expectedRows = NUMBER_OF_MEMBERS;
		
		// get the number of rows in the db
		int actualRows = memberJdbcDaoSupport.countRows();
		List<Member> members = memberJdbcDaoSupport.listMembers();
		
		// Verify that the number of rows is as expected
		// doing twice for consistency
		assertEquals("actual rows is not the same as expected", actualRows, members.size());
		assertEquals("actual rows is not the same as expected", expectedRows, members.size());
		
		// check that list contains not nulls, and id's not empty
		for (Member member : members) {
			assertNotNull("List shouldn't contain  nulls", member);
			assertTrue("id of object isn't correct", member.getId() > 0 && member.getId() < 8);
		}	
	}

	/**
	 * Test method for {@link com.thelibrary.jdbctemplate.dao.impl.MemberJdbcDaoSupport#listMember(java.lang.Integer)}.
	 * Method under test is the method to be used to list down the record from the
	 * Member table which has the book on loan with given book id
	 * Expected output is the member who has the book
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testListMemberWithBookId() {
		// initialize expected values
		
		// book id in question
		int bookId = 3;
		
		// expecting 2 members
		int numberOfExpectedMembers = 2;
		
		// members borrowed the book id 3
		Member expectedMember1 = new Member(6, "Fiona Pascal", "Ballymun", "Dublin 23", 
				"Dublin", "0873530422", 4, 0, true);
		Member expectedMember2 = new Member(3, "Mark Little", "Phibsborough", "Dublin 3", 
				"Dublin", "087924273", 4, 22, true);
	
		
		// list the members
		List<Member> actualMembers = memberJdbcDaoSupport.listMember(bookId);
		
		// check that there are only 2 records as expected
		assertTrue("Number of rows returned is not as expected", actualMembers.size() == numberOfExpectedMembers);
				
		// check the rows
		for (Member actualMember : actualMembers) {
			assertNotNull("List shouldn't contain  nulls", actualMember);
			
			// check correct id range
			assertTrue("id of object isn't correct", actualMember.getId() > 0 && actualMember.getId() < 8);
			
			// check that the id's are just what we'd expect
			assertTrue("id of object isn't correct", actualMember.getId() == expectedMember1.getId() || actualMember.getId() == expectedMember2.getId());
		
			// check that the members are as expected
			if(actualMember.getId() == expectedMember1.getId()) {
				assertEquals("database member id is not the same as expected",
						expectedMember1.getId(), actualMember.getId());
				assertEquals("database member name is not the same as expected",
						expectedMember1.getName(), actualMember.getName());
				assertEquals("database member address1 is not the same as expected",
						expectedMember1.getAddress1(), actualMember.getAddress1());
				assertEquals("database member address2 is not the same as expected",
						expectedMember1.getAddress2(), actualMember.getAddress2());
				assertEquals("database member town is not the same as expected",
						expectedMember1.getTown(), actualMember.getTown());
				assertEquals("database member contact number is not the same as expected",
						expectedMember1.getContactNumber(), actualMember.getContactNumber());
				assertEquals("database member book allowance is not the same as expected",
						expectedMember1.getBookAllowance(), actualMember.getBookAllowance());
				assertTrue("database member balance is not the same as expected",
						expectedMember1.getBalance() == actualMember.getBalance());
				assertEquals("database member active is not the same as expected",
						expectedMember1.isActive(), actualMember.isActive());
			}
			else 
				if (actualMember.getId() == expectedMember2.getId()) {

					assertEquals("database member id is not the same as expected",
							expectedMember2.getId(), actualMember.getId());
					assertEquals("database member name is not the same as expected",
							expectedMember2.getName(), actualMember.getName());
					assertEquals("database member address1 is not the same as expected",
							expectedMember2.getAddress1(), actualMember.getAddress1());
					assertEquals("database member address2 is not the same as expected",
							expectedMember2.getAddress2(), actualMember.getAddress2());
					assertEquals("database member town is not the same as expected",
							expectedMember2.getTown(), actualMember.getTown());
					assertEquals("database member contact number is not the same as expected",
							expectedMember2.getContactNumber(), actualMember.getContactNumber());
					assertEquals("database member book allowance is not the same as expected",
							expectedMember2.getBookAllowance(), actualMember.getBookAllowance());
					assertTrue("database member balance is not the same as expected",
							expectedMember2.getBalance() == actualMember.getBalance());
					assertEquals("database member active is not the same as expected",
							expectedMember2.isActive(), actualMember.isActive());
				}
				else {
					fail("Members are not as was expected"); 
				}
		}
	}

	/**
	 * Test method for {@link com.thelibrary.jdbctemplate.dao.impl.MemberJdbcDaoSupport#listMembers(java.lang.String)}.
	 * Method under test is the method to be used to list down all the records from the
	 * Member table which have the book on loan with given title
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testListMembersWithBookTitle() {
		// initialize expected values
		// book title in question
		final String bookTitle = "book 3";
		
		// expecting 2 members
		int numberOfExpectedMembers = 2;
		
		// members borrowed the book id 3
		Member expectedMember1 = new Member(6, "Fiona Pascal", "Ballymun", "Dublin 23", 
				"Dublin", "0873530422", 4, 0, true);
		Member expectedMember2 = new Member(3, "Mark Little", "Phibsborough", "Dublin 3", 
				"Dublin", "087924273", 4, 22, true);
	
		
		// list the members with the given book title
		List<Member> actualMembers = memberJdbcDaoSupport.listMembers(bookTitle);
		
		// check that there are only 2 records as expected
		assertTrue("Number of rows returned is not as expected", actualMembers.size() == numberOfExpectedMembers);
				
		// check the rows
		for (Member actualMember : actualMembers) {
			assertNotNull("List shouldn't contain  nulls", actualMember);
			
			// check correct id range
			assertTrue("id of object isn't correct", actualMember.getId() > 0 && actualMember.getId() < 8);
			
			// check that the id's are just what we'd expect
			assertTrue("id of object isn't correct", actualMember.getId() == expectedMember1.getId() || actualMember.getId() == expectedMember2.getId());
		
			// check that the members are as expected
			if(actualMember.getId() == expectedMember1.getId()) {
				assertEquals("database member id is not the same as expected",
						expectedMember1.getId(), actualMember.getId());
				assertEquals("database member name is not the same as expected",
						expectedMember1.getName(), actualMember.getName());
				assertEquals("database member address1 is not the same as expected",
						expectedMember1.getAddress1(), actualMember.getAddress1());
				assertEquals("database member address2 is not the same as expected",
						expectedMember1.getAddress2(), actualMember.getAddress2());
				assertEquals("database member town is not the same as expected",
						expectedMember1.getTown(), actualMember.getTown());
				assertEquals("database member contact number is not the same as expected",
						expectedMember1.getContactNumber(), actualMember.getContactNumber());
				assertEquals("database member book allowance is not the same as expected",
						expectedMember1.getBookAllowance(), actualMember.getBookAllowance());
				assertTrue("database member balance is not the same as expected",
						expectedMember1.getBalance() == actualMember.getBalance());
				assertEquals("database member active is not the same as expected",
						expectedMember1.isActive(), actualMember.isActive());
			}
			else 
				if (actualMember.getId() == expectedMember2.getId()) {

					assertEquals("database member id is not the same as expected",
							expectedMember2.getId(), actualMember.getId());
					assertEquals("database member name is not the same as expected",
							expectedMember2.getName(), actualMember.getName());
					assertEquals("database member address1 is not the same as expected",
							expectedMember2.getAddress1(), actualMember.getAddress1());
					assertEquals("database member address2 is not the same as expected",
							expectedMember2.getAddress2(), actualMember.getAddress2());
					assertEquals("database member town is not the same as expected",
							expectedMember2.getTown(), actualMember.getTown());
					assertEquals("database member contact number is not the same as expected",
							expectedMember2.getContactNumber(), actualMember.getContactNumber());
					assertEquals("database member book allowance is not the same as expected",
							expectedMember2.getBookAllowance(), actualMember.getBookAllowance());
					assertTrue("database member balance is not the same as expected",
							expectedMember2.getBalance() == actualMember.getBalance());
					assertEquals("database member active is not the same as expected",
							expectedMember2.isActive(), actualMember.isActive());
				}
				else {
					fail("Members are not as was expected"); 
				}
		}
	}

	/**
	 * Test method for {@link com.thelibrary.jdbctemplate.dao.impl.MemberJdbcDaoSupport#listMembers(java.lang.Integer)}.
	 * This method under test is the method to be used to list down all the records from the
	 * Member table which have less than number passed of books or less
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testListMembersWithBooksLessThan() {
		// initialize expected values
		final int numberOfBooks = 3;
		
		// expecting 4 members
		int numberOfExpectedMembers = 4;
		
		int [] expectedMemberIds = {1, 2, 4, 7};
		
		// list the members with 1, 2 or 3 books on loan
		List<Member> actualMembers = memberJdbcDaoSupport.listMembers(numberOfBooks);
		
		// check that there are only 4 records as expected
		assertTrue("Number of rows returned is not as expected", actualMembers.size() == numberOfExpectedMembers);
				
		// check the rows
		for (Member actualMember : actualMembers) {
			assertNotNull("List shouldn't contain  nulls", actualMember);
			
			// check correct id range
			assertTrue("id of member isn't correct", actualMember.getId() > 0 && actualMember.getId() < 8);
			
			// check that the id's are just what we'd expect
			assertTrue("id of member isn't correct", actualMember.getId() == expectedMemberIds[0] || 
													 actualMember.getId() == expectedMemberIds[1] || 
													 actualMember.getId() == expectedMemberIds[2] || 
													 actualMember.getId() == expectedMemberIds[3] );
		}
	}

	/**
	 * Test method for
	 * {@link com.thelibrary.jdbctemplate.dao.impl.BookJdbcDaoSupport#countRows()}
	 * .
	 * Expected output is that the number of rows will be the same as expected
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testCountRows() {
		
		// book rows in the database
		int expectedRows = NUMBER_OF_MEMBERS;
		
		// get the number of rows in the db
		int actualRows = memberJdbcDaoSupport.countRows();
		
		// Verify that the number of rows his as expected
		assertEquals("actual rows is not the same as expected", expectedRows, actualRows);
	}
}
