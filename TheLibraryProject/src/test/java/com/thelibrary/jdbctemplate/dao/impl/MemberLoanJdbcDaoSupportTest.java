package com.thelibrary.jdbctemplate.dao.impl;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.thelibrary.domain.Book;
import com.thelibrary.domain.Member;
import com.thelibrary.domain.MemberLoan;
import com.thelibrary.interfaces.impl.LoanImpl;

/**
 * @author adam abdulrehman R00118124
 * 
 * JUnit Test for MemberLoanJdbcDaoSupport
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:configuration.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class })
public class MemberLoanJdbcDaoSupportTest {

	// since the applicationContext is autowired there is no need for
	// setting up the bean
	@Autowired
	ApplicationContext applicationContext;

	// since the MemberJdbcDaoSupport is autowired there is no need for
	// setting up the bean
	@Autowired
	MemberLoanJdbcDaoSupport memberLoanJdbcDaoSupport;

	@Autowired
	MemberJdbcDaoSupport memberJdbcDaoSupport;
	
	@Autowired
	BookJdbcDaoSupport bookJdbcDaoSupport;
	
	static final int NUMBER_OF_MEMBERS = 7;
	static final int NUMBER_OF_BOOKS = 10;
	static final int NUMBER_OF_LOANS = 21;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for public void createMemberLoan(Integer memberId, LoanImpl loan);
	 * This is the test method for method that creates a record in the Member_loans_Book table.
     * 
     * The expected output is that the newly created memberLoan is inserted into the database.
     * The database rows should increase by one and the details of the new memberLoan
     * should exist in the table
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testCreateMemberLoan() {
		
		/******************** Member creation and testing ****************************/
		// member variables to be used for creation
		final String actualName = "Harry Potter";
		final String actualAddress1 = "45";
		final String actualAddress2 = "willowbog farm road";
		final String actualTown = "Athy";
		final String actualContactNumber = "654321";
		final int actualBookAllowance = 6;
		final double actualBalance = 32.89;
		final boolean actualActive = true;

		// get the number of rows in the db
		int rowCountBeforeCreate = memberJdbcDaoSupport.countRows();

		// create the member
		KeyHolder ids = memberJdbcDaoSupport.createMember(actualName, actualAddress1, actualAddress2, actualTown, actualContactNumber, actualBookAllowance, actualBalance, actualActive);

		// get the number of rows in the db
		int actualRowCount = memberJdbcDaoSupport.countRows();
		assertEquals("Number of rows is not as expected",
				rowCountBeforeCreate + 1, actualRowCount);

		// test that a generated key is returned
		assertNotNull("returned keys is null", ids.getKey());

		// test that the rows have increased by one
		assertEquals("rows don't match as expected", NUMBER_OF_MEMBERS + 1,
				memberJdbcDaoSupport.countRows());

		// test the member inserted
		Member member = memberJdbcDaoSupport.getMember(ids.getKey().intValue());
		assertNotNull("inserted member is null", member);
		assertEquals("inserted member name is not the same as expected",
				actualName, member.getName());
		assertEquals("inserted member address1 is not the same as expected",
				actualAddress1, member.getAddress1());
		assertEquals("inserted member address2 is not the same as expected",
				actualAddress2, member.getAddress2());
		assertEquals("inserted member town is not the same as expected",
				actualTown, member.getTown());
		assertEquals("inserted member contact number is not the same as expected",
				actualContactNumber, member.getContactNumber());
		assertEquals("inserted member book allowance is not the same as expected",
				actualBookAllowance, member.getBookAllowance());
		assertTrue("inserted member balance is not the same as expected",
				actualBalance == member.getBalance());
		assertEquals("inserted member active is not the same as expected",
				actualActive, member.isActive());

		/******************** book creation and testing ****************************/
		// book values to be used for creation
		final String actualTitle = "Lord Of the ring";
		final String actualAuthor = "JRTolkien";
		final String actualPublisher = "APress";

		Calendar calendar = Calendar.getInstance();
		calendar.set(2012, 12, 12, 0, 0, 0);
		java.util.Date actualPublicationDate = calendar.getTime();

		final String actualIsbn = "112333";
		final boolean actualAvailability = true;

		// get the number of rows in the db
		rowCountBeforeCreate = bookJdbcDaoSupport.countRows();
			
		// create the book
		ids = null;
		
		try {
			 ids = bookJdbcDaoSupport.createBook(actualTitle,
						actualAuthor, actualPublisher, actualPublicationDate,
						actualIsbn, actualAvailability);
			
		} catch (DataAccessException ex) {
			fail("getting the book threw an exception when it was expected not to");
		}

		// get the number of rows in the db
		actualRowCount = bookJdbcDaoSupport.countRows();
		assertEquals("Number of rows is not as expected",
				rowCountBeforeCreate + 1, actualRowCount);

		// test that a key holder is not null
		assertNotNull("keys is null", ids);
				
		// test that a generated key is returned
		assertNotNull("returned keys is null", ids.getKey());

		// test that the rows have increased by one
		assertEquals("rows don't much", NUMBER_OF_BOOKS + 1,
				bookJdbcDaoSupport.countRows());

		// test the book inserted
		Book book = bookJdbcDaoSupport.getBook(ids.getKey().intValue());
		assertNotNull("inserted book is null", book);
		assertEquals("inserted book title is not the same as expected",
				actualTitle, book.getTitle());
		assertEquals("inserted book author is not the same as expected",
				actualAuthor, book.getAuthor());
		assertEquals("inserted book publisher is not the same as expected",
				actualPublisher, book.getPublisher());
		assertEquals("inserted publication date is not the same as expected",
				actualPublicationDate.toString(), book.getPublicationDate()
						.toString());
		assertEquals("inserted book isbn is not the same as expected",
				actualIsbn, book.getIsbn());
		assertEquals("inserted book availabilty is not the same as expected",
				actualAvailability, book.isAvailable());
		
		

		/******************* create the memberLoan *******************************/
		calendar = Calendar.getInstance();
		calendar.set(2012, 12, 31, 0, 0, 0);
		java.util.Date actualLoanDate = calendar.getTime();
		
		LoanImpl loan = new LoanImpl(book, actualLoanDate);
		
		// get the number of rows in the db
		rowCountBeforeCreate = memberLoanJdbcDaoSupport.countRows();
				
		try {
			memberLoanJdbcDaoSupport.createMemberLoan(member.getId(), loan);
			
		} catch (DataAccessException ex) {
			fail("getting the memberLoan threw an exception when it was expected not to");
		}
		

		// get the number of rows in the db
		actualRowCount = memberLoanJdbcDaoSupport.countRows();
		assertEquals("Number of rows is not as expected",
				rowCountBeforeCreate + 1, actualRowCount);

		// test that a generated key is returned
		assertNotNull("returned keys is null", ids.getKey());

		// test that the rows have increased by one
		assertEquals("rows don't match as expected", NUMBER_OF_LOANS + 1,
				memberLoanJdbcDaoSupport.countRows());

		// test the memberLoan inserted
		List<MemberLoan> memberLoans = memberLoanJdbcDaoSupport.listMemberLoansWithMember(member.getId());
		assertNotNull("inserted memberLoan is null", memberLoans);
		assertEquals("inserted memberLoan is not one", 1,  memberLoans.size());
		MemberLoan memberLoan = memberLoans.get(0);
		assertEquals("inserted memberLoan name is not the same as expected",
				actualName, memberLoan.getName());
		assertEquals("inserted member address1 is not the same as expected",
				actualAddress1, memberLoan.getAddress1());
		assertEquals("inserted member address2 is not the same as expected",
				actualAddress2, memberLoan.getAddress2());
		assertEquals("inserted member town is not the same as expected",
				actualTown, memberLoan.getTown());
		assertEquals("inserted member contact number is not the same as expected",
				actualContactNumber, memberLoan.getContactNumber());
		assertEquals("inserted member book allowance is not the same as expected",
				actualBookAllowance, memberLoan.getBookAllowance());
		assertTrue("inserted member balance is not the same as expected",
				actualBalance == memberLoan.getBalance());
		assertEquals("inserted member active is not the same as expected",
				actualActive, memberLoan.isActive());
		
		assertEquals("inserted book title is not the same as expected",
				actualTitle, memberLoan.getLoan().getTitle());
		assertEquals("inserted book author is not the same as expected",
				actualAuthor, memberLoan.getLoan().getAuthor());
		assertEquals("inserted book publisher is not the same as expected",
				actualPublisher, memberLoan.getLoan().getPublisher());
		assertEquals("inserted publication date is not the same as expected",
				actualPublicationDate.toString(), memberLoan.getLoan().getPublicationDate()
						.toString());
		assertEquals("inserted book isbn is not the same as expected",
				actualIsbn, memberLoan.getLoan().getIsbn());
		assertEquals("inserted book availabilty is not the same as expected",
				actualAvailability, memberLoan.getLoan().isAvailable());
		
		assertEquals("inserted loan date is not the same as expected",
				actualLoanDate.toString(), memberLoan.getLoan().getLoanDate().toString());
	}

	/**
	 * Test method for public int deleteMemberLoanWithMember(Integer memberId);
	 * This is the test method for the method to be used to delete all records in 
	 * the Member_loans_Book table given a member_id. 
	 * Returns the number of rows affected
	 *
     * The expected output is that the memberLoans for members with id memberId
     * are deleted from the database.
     * The database rows should decrease by one and the details of the memberLoan(s)
     * should not exist in the table
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testDeleteMemberLoanWithMember() {
		// member id of member under delete test
		final int memberId = 2;

		// check if the memberLoan exists first and get the details of the memberLoan
		List<MemberLoan> memberLoans = memberLoanJdbcDaoSupport.listMemberLoansWithMember(memberId);

		// verify that we have the one and only memberLoan
		assertTrue("memberLoan number is not as expected", memberLoans.size() == 1);
				
		// verify that we have the memberLoan
		assertNotNull("memberLoan is null", memberLoans);

		// verify that an memberId is present and same as what we asked for
		assertTrue("member id is not as expected", memberLoans.get(0).getId() == memberId);

		// get the number of rows in the db
		int rowCountBeforeDelete = memberLoanJdbcDaoSupport.countRows();

		// Delete the member passing in the member id
		int affectedRows = memberLoanJdbcDaoSupport.deleteMemberLoanWithMember(memberId);

		// get the number of rows in the db
		int actualRowCount = memberLoanJdbcDaoSupport.countRows();
		assertEquals("Number of rows is not as expected",
				rowCountBeforeDelete - 1, actualRowCount);

		// Verify that 1 row has been affected since we just have 1 entry for that id
		assertEquals("affected rows is not the same as expected", 1,
				affectedRows);

		// verify that the memberLoan does not now exist anymore in the database
		// after deletion by getting the memberLoan
		List<MemberLoan> deletedMemberLoans = null;
		deletedMemberLoans = memberLoanJdbcDaoSupport.listMemberLoansWithMember(memberId);
		assertTrue("deleted memberLoan still exists and they shouldn't", deletedMemberLoans.isEmpty());
	}

	/**
	 * Test method for public int deleteMemberLoanWithBook(Integer bookId);
	 * This is the test method for the method to be used to delete all records in 
	 * the Member_loans_Book table given a bookid. 
	 * Returns the number of rows affected
	 *
     * The expected output is that the memberLoans for books with id bookId
     * are deleted from the database.
     * The database rows should decrease by one and the details of the memberLoan(s)
     * should not exist in the table
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testDeleteMemberLoanWithBook() {
		// member id of member under delete test
		final int bookId = 7;

		// check if the memberLoan exists first and get the details of the memberLoan
		List<MemberLoan> memberLoans = memberLoanJdbcDaoSupport.listMemberLoansWithBook(bookId);

		// verify that we have the memberLoan
		assertNotNull("memberLoan is null", memberLoans);

		// verify that we have the one and only memberLoan
		assertTrue("memberLoan number is not as expected", memberLoans.size() == 1);
				
		// verify that an memberId is present and same as what we asked for
		assertTrue("book id is not as expected", memberLoans.get(0).getLoan().getId() == bookId);

		// get the number of rows in the db
		int rowCountBeforeDelete = memberLoanJdbcDaoSupport.countRows();

		// Delete the member passing in the book id
		int affectedRows = memberLoanJdbcDaoSupport.deleteMemberLoanWithBook(bookId);

		// get the number of rows in the db
		int actualRowCount = memberLoanJdbcDaoSupport.countRows();
		assertEquals("Number of rows is not as expected",
				rowCountBeforeDelete - 1, actualRowCount);

		// Verify that 1 row has been affected since we just have 1 entry for that id
		assertEquals("affected rows is not the same as expected", 1,
				affectedRows);

		// verify that the memberLoan does not now exist anymore in the database
		// after deletion by getting the memberLoan
		List<MemberLoan> deletedMemberLoans = null;
		deletedMemberLoans = memberLoanJdbcDaoSupport.listMemberLoansWithBook(bookId);
		assertTrue("deleted memberLoan still exists and they shouldn't", deletedMemberLoans.isEmpty());
	}

	/**
	 * Test method for public List<MemberLoan> listMemberLoansWithMember(Integer memberId);
	 * This is the test method for the method to be used to list down all the records from the
	 * Member_loans_Book table for the loans of the member with the given id.
     * 
     * The expected output is that a List of the memberLoans for the memberLoans with the
     * particular Member will be returned
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testListMemberLoansWithMember() {
		// member id of member under delete test
		final int expectedBookId = 9;
        final int expectedMemberId = 2;
        
		// check if the memberLoan exists first and get the details of the memberLoan
		List<MemberLoan> memberLoans = memberLoanJdbcDaoSupport.listMemberLoansWithMember(expectedMemberId);

		// verify that we have the memberLoan
		assertNotNull("memberLoan is null", memberLoans);

		// verify that we have the one and only memberLoan
		assertEquals("memberLoan number is not as expected", 1, memberLoans.size());
				
		// verify that an MemberId is present and same as what we expected
		assertEquals("Member id is not as expected", expectedMemberId, memberLoans.get(0).getId());
				
		// verify that an bookId is present and same as what we asked for
		assertTrue("book id is not as expected", memberLoans.get(0).getLoan().getId() == expectedBookId);

		// get the number of rows in the db
		int rowCountBeforeDelete = memberLoanJdbcDaoSupport.countRows();

		// Delete the memberLoan passing in the book id
		int affectedRows = memberLoanJdbcDaoSupport.deleteMemberLoanWithBook(expectedBookId);

		// get the number of rows in the db
		int actualRowCount = memberLoanJdbcDaoSupport.countRows();
		assertEquals("Number of rows is not as expected",
				rowCountBeforeDelete - 1, actualRowCount);

		// Verify that 1 row has been affected since we just have 1 entry for that id
		assertEquals("affected rows is not the same as expected", 1,
				affectedRows);

		// verify that the memberLoan does not now exist anymore in the database
		// after deletion by getting the memberLoan
		List<MemberLoan> deletedMemberLoans = null;
		deletedMemberLoans = memberLoanJdbcDaoSupport.listMemberLoansWithMember(expectedMemberId);
		assertTrue("deleted memberLoan still exists and they shouldn't", deletedMemberLoans.isEmpty());
	}

	/**
	 * Test method for public List<MemberLoan> listMemberLoansWithBook(Integer bookId);
	 * This is the test method for the method that is be used to list down all the records from the
	 * Member_loans_Book table for the loans of the member with the given id.
     * 
     * The expected output is that a List of the memberLoans for the memberLoans with the
     * particular book will be returned
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testListMemberLoansWithBook() {
		// member id of member under delete test
		final int expectedBookId = 7;
        final int expectedMemberId = 3;
        
		// check if the memberLoan exists first and get the details of the memberLoan
		List<MemberLoan> memberLoans = memberLoanJdbcDaoSupport.listMemberLoansWithBook(expectedBookId);

		// verify that we have the memberLoan
		assertNotNull("memberLoan is null", memberLoans);

		// verify that we have the one and only memberLoan
		assertTrue("memberLoan number is not as expected", memberLoans.size() == 1);
				
		// verify that an MemberId is present and same as what we expected
		assertEquals("Member id is not as expected", expectedMemberId, memberLoans.get(0).getId());
				
		// verify that an bookId is present and same as what we asked for
		assertTrue("book id is not as expected", memberLoans.get(0).getLoan().getId() == expectedBookId);

		// get the number of rows in the db
		int rowCountBeforeDelete = memberLoanJdbcDaoSupport.countRows();

		// Delete the memberLoan passing in the book id
		int affectedRows = memberLoanJdbcDaoSupport.deleteMemberLoanWithBook(expectedBookId);

		// get the number of rows in the db
		int actualRowCount = memberLoanJdbcDaoSupport.countRows();
		assertEquals("Number of rows is not as expected",
				rowCountBeforeDelete - 1, actualRowCount);

		// Verify that 1 row has been affected since we just have 1 entry for that id
		assertEquals("affected rows is not the same as expected", 1,
				affectedRows);

		// verify that the memberLoan does not now exist anymore in the database
		// after deletion by getting the memberLoan
		List<MemberLoan> deletedMemberLoans = null;
		deletedMemberLoans = memberLoanJdbcDaoSupport.listMemberLoansWithBook(expectedBookId);
		assertTrue("deleted memberLoan still exists and they shouldn't", deletedMemberLoans.isEmpty());
	}

	/**
	 * Test method for public void listMemberLoans();
	 * This is the test method for the method to be used to list down all the records from the
	 * Member_loans_Book table .
     * 
     * The number of database rows should be as expected
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testListMemberLoans() {
		// memberLoan rows in the database
		int expectedRows = NUMBER_OF_LOANS;
		
		// get the number of rows in the db
		int actualRows = memberLoanJdbcDaoSupport.countRows();
		List<MemberLoan> memberloans = memberLoanJdbcDaoSupport.listMemberLoans();
		
		// Verify that the number of rows is as expected
		// doing twice for consistency
		assertEquals("actual rows is not the same as expected", actualRows, memberloans.size());
		assertEquals("actual rows is not the same as expected", expectedRows, memberloans.size());
		
		// check that list contains not nulls, and id's not empty
		for (Member memberLoan : memberloans) {
			assertNotNull("List shouldn't contain  nulls", memberLoan);
			assertTrue("id of object isn't correct", memberLoan.getId() > 0 && memberLoan.getId() < 8);
		}	
	}

	/**
	 * Test method for countRows()
	 * 
	 * Expected output is that the number of rows will be the same as expected
	 */
	@Test
	@DatabaseSetup(value = "classpath:databaseEntries.xml", type = DatabaseOperation.CLEAN_INSERT)
	public void testCountRows() {
		// memberLoan rows in the database
		int expectedRows = NUMBER_OF_LOANS;
		
		// get the number of rows in the db
		int actualRows = memberLoanJdbcDaoSupport.countRows();
		
		// Verify that the number of rows his as expected
		assertEquals("actual rows is not the same as expected", expectedRows, actualRows);
	}

}
